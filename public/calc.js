/// <reference types='jquery' />

const operations = {
  JMPNEAR: "JMPN",
  JMPSHORT: "JMPS",
  JZ: "JZ",
  JZSHORT: "JZS",
  JNZ: "JNZ",
  JNZSHORT: "JNZS",
  CALL: "CALL",
  CUSTOM: "CUSTOM",
}

$('#src').on('change paste keyup', () => forceUpdate());
$('#dest').on('change paste keyup', () => forceUpdate());
$('#custopcodes').on('change paste keyup', () => forceUpdate());
$('#shortaddress').change(() => forceUpdate());

$('#CONVERTIN').on('change paste keyup', function () {
  let val = $(this).val();
  let isArray = val.includes('0x');
  if (!val) {
    $('#CONVERTOUT').val('Insert 31 E0 or { 0x31, 0xE0 } into IN to convert between them :)');
  } else if (isArray) {
    val = val.replace(/0x/g, '').replace(/,/g, '').replace(/[{}]/g, '');
    $('#CONVERTOUT').val(val.trim());
  } else {
    let newval = '{ 0x';
    newval += val.trim().replace(/\s/g, ', 0x') + ' }';
    $('#CONVERTOUT').val(newval.trim());
  }
}).trigger('change');

function forceUpdate() {
  let offset = 0;
  let outVal = '';
  let src = $('#src').val();
  let dest = $('#dest').val();
  let op = $('#operation').next().val();

  if (op === operations.CUSTOM)
    $('#customopcodes').slideDown({
      start: function () {
        $(this).css({
          display: "-webkit-box"
        })
      }
    });
  else
    $('#customopcodes').slideUp();

  regexp = /^[0-9a-fA-F]+$/;

  if (parseInt(dest, 16) > parseInt('FFFFFFFF', 16)) {
    $('#dest').addClass('is-bad').next().addClass('is-bad');
    outVal = 'DEST > FFFFFFFF';
    dest = 0;
  }
  else if (dest.replace(/0x/, '') && !regexp.test(dest.replace(/0x/, ''))) {
    $('#dest').addClass('is-bad').next().addClass('is-bad');
    outVal = 'DEST is NaN!';
    dest = 0;
  }
  else
    $('#dest').removeClass('is-bad').next().removeClass('is-bad');

  if (parseInt(src, 16) > parseInt('FFFFFFFF', 16)) {
    $('#src').addClass('is-bad').next().addClass('is-bad');
    outVal = 'SRC > FFFFFFFF';
    src = 0;
  }
  else if (src.replace(/0x/, '') && !regexp.test(src.replace(/0x/, ''))) {
    $('#src').addClass('is-bad').next().addClass('is-bad');
    outVal = 'SRC is NaN!';
    src = 0;
  }
  else
    $('#src').removeClass('is-bad').next().removeClass('is-bad');

  if (src && dest) {
    let userIsAnIdiot = false;
    let custshort = $('#shortaddress').prop('checked');
    let diff = parseInt(src, 16) - parseInt(dest, 16);
    let reverse = diff >= 1;
    diff = offset = Math.abs(diff);
    let opsize = 0;
    switch (op) {
      case operations.JMPNEAR:
        outVal = 'E9';
        opsize = 5;
        break;
      case operations.JMPSHORT:
        outVal = 'EB';
        opsize = 2;
        break;
      case operations.JZ:
        outVal = '0F 84';
        opsize = 6;
        break;
      case operations.JZSHORT:
        outVal = '74';
        opsize = 2;
        break;
      case operations.JNZ:
        outVal = '0F 85';
        opsize = 6;
        break;
      case operations.JNZSHORT:
        outVal = '75';
        opsize = 2;
        break;
      case operations.CALL:
        outVal = 'E8';
        opsize = 5;
        break;
      case operations.CUSTOM:
        let custcodes = outVal = $('#custopcodes').val();
        let custcodeslength = custcodes.replace(/ /g, '').length;
        if (!custcodeslength)
          userIsAnIdiot = true;
        if (custcodeslength % 2 != 0 || !custcodeslength)
          $('#custopcodes').addClass('is-bad').removeClass('is-good').next().addClass('is-bad').removeClass('is-good');
        else
          $('#custopcodes').removeClass('is-bad').addClass('is-good').next().removeClass('is-bad').addClass('is-good');
        opsize = custcodes.replace(/ /g, '').length / 2;
        opsize += custshort ? 1 : 4;
        break;
    }
    if (reverse) {
      if (opsize < 5 || op === operations.CUSTOM && custshort) {
        diff = parseInt('FF', 16) - diff + 1;
        if (diff - opsize < parseInt('80', 16))
          userIsAnIdiot = true;
      } else {
        diff = parseInt('FFFFFFFF', 16) - diff + 1;
        if (diff - opsize < (parseInt('80FFFFFF', 16)))
          userIsAnIdiot = true;
      }
    } else {
      if (opsize < 5 || op === operations.CUSTOM && custshort) {
        if (diff - opsize > parseInt('7F', 16))
          userIsAnIdiot = true;
      } else {
        if (diff - opsize > (parseInt('7FFFFFFF', 16)))
          userIsAnIdiot = true;
      }
    }
    diff -= opsize;

    if (diff < 1)
      userIsAnIdiot = true;

    let temp = '';
    let hex = diff.toString(16).toUpperCase();
    let short = opsize < 5 || op === operations.CUSTOM && custshort;
    if (userIsAnIdiot) {
      outVal = 'NOT POSSIBLE';
    } else {
      if (hex.length < (short ? 2 : 8)) {
        let missing = (short ? 2 : 8) - hex.length;
        while (temp.length < missing)
          temp += '0';
      }
      temp += hex;
      if (!short) {
        hex = temp;
        temp = '';
        for (let i = 6; i > -2; i -= 2) {
          temp += hex.substr(i, 2) + ' ';
        }
      }
      outVal += ' ' + temp;
    }
  } else
    $('#custopcodes').removeClass('is-bad').removeClass('is-good').next().removeClass('is-bad').removeClass('is-good');
  $('#outoffset').text('offset: 0x' + offset.toString(16).toUpperCase());
  $('#hexasmout').val(outVal ? outVal : '');
}

$('#operation').change(function () {
  forceUpdate();
});